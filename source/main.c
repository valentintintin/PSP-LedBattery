#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "var.h"
#include <pspkernel.h>
#include <pspdebug.h>
#include <pspdisplay.h>
#include <pspctrl.h> 
#include <pspsyscon.h>
#include <psppower.h>

#define printf pspDebugScreenPrintf
#define power 2
#define allume 1
#define eteint 0

PSP_MODULE_INFO("LedBatterie", 0x1000, 1, 0);
PSP_MAIN_THREAD_ATTR(0);

int main_thread(SceSize args, void *argp)
{	
	pspDebugScreenInit();
	sceKernelDelayThread(3000000);
	while(1)
	{
	SceCtrlData pad;
	sceCtrlPeekBufferPositive(&pad, 1);
	pspDebugScreenSetXY(0, 0);
	if (pad.Buttons & PSP_CTRL_HOLD)
	{
		sceSysconCtrlLED(power, eteint);
	}
	else
	{
		if (scePowerGetBatteryLifePercent() >= 90 && scePowerGetBatteryLifePercent() <= 100)
		{
		    sceSysconCtrlLED(power, allume);
			sceKernelDelayThread(3000000);
			sceSysconCtrlLED(power, eteint);
			sceKernelDelayThread(3000000);
			sceSysconCtrlLED(power, allume);
		}
		else if (scePowerGetBatteryLifePercent() >= 80 && scePowerGetBatteryLifePercent() <= 90)
		{
			sceSysconCtrlLED(power, allume);
			sceKernelDelayThread(2500000);
			sceSysconCtrlLED(power, eteint);
			sceKernelDelayThread(2500000);
			sceSysconCtrlLED(power, allume);
		}
		else if (scePowerGetBatteryLifePercent() >= 70 && scePowerGetBatteryLifePercent() <= 80)
		{
		    sceSysconCtrlLED(power, allume);
			sceKernelDelayThread(2000000);
			sceSysconCtrlLED(power, eteint);
			sceKernelDelayThread(2000000);
			sceSysconCtrlLED(power, allume);
		}
		else if (scePowerGetBatteryLifePercent() >= 60 && scePowerGetBatteryLifePercent() <= 70)
		{
			sceSysconCtrlLED(power, allume);
			sceKernelDelayThread(1500000);
			sceSysconCtrlLED(power, eteint);
			sceKernelDelayThread(1500000);
			sceSysconCtrlLED(power, allume);
		}
		else if (scePowerGetBatteryLifePercent() >= 50 && scePowerGetBatteryLifePercent() <= 60)
		{
		    	sceSysconCtrlLED(power, allume);
			sceKernelDelayThread(1000000);
			sceSysconCtrlLED(power, eteint);
			sceKernelDelayThread(1000000);
			sceSysconCtrlLED(power, allume);
		}
		else if (scePowerGetBatteryLifePercent() >= 40 && scePowerGetBatteryLifePercent() <= 50)
		{
			sceSysconCtrlLED(power, allume);
			sceKernelDelayThread(75000);
			sceSysconCtrlLED(power, eteint);
			sceKernelDelayThread(75000);
			sceSysconCtrlLED(power, allume);
		}
		else if (scePowerGetBatteryLifePercent() >= 30 && scePowerGetBatteryLifePercent() <= 20)
		{
			sceSysconCtrlLED(power, allume);
			sceKernelDelayThread(5000);
			sceSysconCtrlLED(power, eteint);
			sceKernelDelayThread(5000);
			sceSysconCtrlLED(power, allume);
		}
		else if (scePowerGetBatteryLifePercent() >= 20 && scePowerGetBatteryLifePercent() <= 30)
		{
			sceSysconCtrlLED(power, allume);
			sceKernelDelayThread(2500);
			sceSysconCtrlLED(power, eteint);
			sceKernelDelayThread(2500);
			sceSysconCtrlLED(power, allume);
		}
		else if (scePowerGetBatteryLifePercent() >= 10 && scePowerGetBatteryLifePercent() <= 20)
		{
			sceSysconCtrlLED(power, allume);
			sceKernelDelayThread(1000);
			sceSysconCtrlLED(power, eteint);
			sceKernelDelayThread(1000);
			sceSysconCtrlLED(power, allume);
		}
		else if (scePowerGetBatteryLifePercent() >= 5 && scePowerGetBatteryLifePercent() <= 10)
		{
			sceSysconCtrlLED(power, allume);
			sceKernelDelayThread(500);
			sceSysconCtrlLED(power, eteint);
			sceKernelDelayThread(500);
			sceSysconCtrlLED(power, allume);
		}
		
		else if (scePowerGetBatteryLifePercent() >= 3 && scePowerGetBatteryLifePercent() <= 5)
		{
			sceSysconCtrlLED(power, allume);
			sceKernelDelayThread(50);
			sceSysconCtrlLED(power, eteint);
			sceKernelDelayThread(50);
			sceSysconCtrlLED(power, allume);
		}
		
		else if (scePowerGetBatteryLifePercent() >= 0 && scePowerGetBatteryLifePercent() <= 3)
		{
			sceSysconCtrlLED(power, allume);
			sceKernelDelayThread(10);
			sceSysconCtrlLED(power, eteint);
			sceKernelDelayThread(10);
			sceSysconCtrlLED(power, allume);
		}
	}
	
    	sceKernelDelayThread(100000);
	}
	return 0;
}

int module_start(SceSize args, void *argp)
{
	int thid = sceKernelCreateThread("LEDControl", main_thread, 0x30, 0x1000, 0, NULL);
	if(thid >= 0) sceKernelStartThread(thid, args, argp);

	return 0;
}

int module_stop(SceSize args, void *argp)
{
   return 0;
}
